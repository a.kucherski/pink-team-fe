import { TypeIcoPipe } from './type-ico.pipe';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { TypeImgPipe } from './type-img.pipe';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [HeaderComponent, TypeImgPipe, TypeIcoPipe],
  exports: [
    HeaderComponent,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    MatDialogModule,
    TypeImgPipe,
    TypeIcoPipe,
    MatIconModule,
    CommonModule,
  ],
  imports: [RouterModule, ReactiveFormsModule, FormsModule, CommonModule],
})
export class SharedModule {}
