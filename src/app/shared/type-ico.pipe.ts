import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeico',
})
export class TypeIcoPipe implements PipeTransform {
  transform(value: string, ...args: any[]) {
    switch (value) {
      case 'Paper':
        return 'mapbox-icon-paper.png';
      case 'Plastic':
        return 'mapbox-icon-plastic.png';
      case 'Metal':
        return 'mapbox-icon-metal.png';
      case 'Glass':
        return 'mapbox-icon-glass.png';
      case 'Clothes':
        return 'mapbox-icon-clothes.png';
      case 'Organic':
        return 'mapbox-icon-organic.png';
      case 'E-waste':
        return 'mapbox-icon-e-waste.png';
      case 'Light-bulbs':
        return 'mapbox-icon-light-bulbs.png';
      case 'Batteries':
        return 'mapbox-icon-batteries.png';
      default:
        return '';
    }
  }
}
