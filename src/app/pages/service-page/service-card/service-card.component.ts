import { AuthService } from 'src/app/services/auth.service';
import { Service } from './../../../shared/interfaces';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-card',
  templateUrl: './service-card.component.html',
  styleUrls: ['./service-card.component.scss']
})
export class ServiceCardComponent implements OnInit {

  @Input() service!: Service
  
  favorite = false
  

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(user => {
      console.log(user);
      console.log(this.service);
      
      this.favorite = !!user.favorites.find(favorite => favorite.service_id == this.service.service_id)
      console.log(this.favorite);
      
    })
  }

  favoriteToggle() {
    
  }

}
