import { MarkerService } from './../../../services/marker.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {
  @Input() number!: string | undefined

  constructor(private markerService: MarkerService) { }
  
  whatsapp() {
    window.open('http://wa.me/' + this.number)
   }

}
