import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';

import { ServicePageRoutingModule } from './service-page-routing.module';
import { ServicePageComponent } from './service-page.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { TypesComponent } from './types/types.component';
import { ServiceCardComponent } from './service-card/service-card.component';



@NgModule({
  declarations: [
    ServicePageComponent,
    ContactsComponent,
    ScheduleComponent,
    TypesComponent,
    ServiceCardComponent,
  ],
  imports: [
    SharedModule,
    ServicePageRoutingModule
  ]
})
export class ServicePageModule { }
